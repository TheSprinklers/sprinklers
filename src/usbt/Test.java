package usbt;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.custom.ViewForm;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.usb4java.Device;

public class Test
  {

    protected Shell shell;

    private USBServer usbServer;
    /**
     * Launch the application.
     * @param args
     */
    public static void main(String[] args)
      {
        try
          {
            Test window = new Test();
            window.open();
          } 
        catch (Exception e)
          {
            e.printStackTrace();
          }
      }

    /**
     * Open the window.
     */
    public void open()
      {
        Display display = Display.getDefault();
        createContents();
        shell.open();
        shell.layout();
        usbServer = new USBServer();
        usbServer.init();
        
        shell.addListener(SWT.Close, new Listener() {
          public void handleEvent(Event event) {
            System.out.println("Closing");
            shell.setVisible(false);            
            usbServer.destruct();
          }
        });
        
        
        while (!shell.isDisposed())
          {
            if (!display.readAndDispatch())
              {
                display.sleep();
              }
          }
                
      }

    /**
     * Create contents of the window.
     */
    protected void createContents()
      {
        shell = new Shell();
        shell.setSize(564, 422);
        shell.setText("Find USB Device");
        shell.setLayout(new FillLayout(SWT.HORIZONTAL));
        
        Composite composite = new Composite(shell, SWT.NONE);
       
        StyledText styledText = new StyledText(composite, SWT.BORDER);
        styledText.setBounds(10, 10, 528, 333);
        
        //btnNewButton
        Button btnRead = new Button(composite, SWT.NONE);
        
        btnRead.addSelectionListener(new SelectionAdapter() 
          {
            @Override
            public void widgetSelected(SelectionEvent e) {
            }
          });
        btnRead.addListener(SWT.Selection, new Listener() {
          public void handleEvent(Event e) {
            switch (e.type) {
            case SWT.Selection:
              short vendor = 2;
              short vendorId = 3;
              Device returnDevice = usbServer.findDevice(vendor,vendorId);
              
              styledText.setText("read");
              break;
            }
          }
        });

                
        btnRead.setBounds(375, 349, 75, 25);
        btnRead.setText("Read");
       
        
        Button btnClear = new Button(composite, SWT.NONE);
        btnClear.setBounds(456, 349, 75, 25);
        btnClear.setText("Clear");
        
        btnClear.addListener(SWT.Selection, new Listener() {
          public void handleEvent(Event e) {
            switch (e.type) {
            case SWT.Selection:
              styledText.setText("");
              break;
            }
          }
        });
        
      }
  }
